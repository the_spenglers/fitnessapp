package edu.csun.a380group5.fitnessapp.WorkoutGenerator;
/*
*
* The Fitness App
*  Group 5
* Timothy Spengler, Joshua Benz, Kodi Winterer, Alex Melendez-White
* Comp 380 Spring 2017
*
*/
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import edu.csun.a380group5.fitnessapp.MainActivity;
import edu.csun.a380group5.fitnessapp.R;
import edu.csun.a380group5.fitnessapp.Utils;

public class ExerciseDataActivity extends AppCompatActivity implements View.OnClickListener {

    private ExerciseStruct exerciseStruct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_data);

        Button button_home = (Button) findViewById(R.id.button_home);
        button_home.setOnClickListener(this);

        Button button_webview = (Button) findViewById(R.id.button_webview);
        button_webview.setOnClickListener(this);
        Toolbar toolbar  = (Toolbar)  findViewById(R.id.toolbar);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {

            exerciseStruct = bundle.getParcelable("exerciseData");
            TextView name = (TextView) findViewById(R.id.textView_name);
            name.setText("Exercise Name: " + exerciseStruct.getName());

            TextView muscle = (TextView) findViewById(R.id.textView_muscle);
            muscle.setText("Muslce Group: " + exerciseStruct.getBodyPart());

        }
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch(v.getId()) {
            case R.id.button_home:
                intent = new Intent(v.getContext(), MainActivity.class);
                v.getContext().startActivity(intent);
                break;
            case R.id.button_webview:
                if(Utils.isNetworkAvailable(this)) {
                    intent = new Intent(this, YoutubeActivity.class);
                    intent.putExtra("exerciseName", exerciseStruct.getName());
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "No Network Connection!", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;
        }
    }
}
