package edu.csun.a380group5.fitnessapp.CalorieTracker;
/*
*
* The Fitness App
*  Group 5
* Timothy Spengler, Joshua Benz, Kodi Winterer, Alex Melendez-White
* Comp 380 Spring 2017
*
*/
//stores item name and amount of calories


public class ItemFood {
    private String name;
    private int cal;
    private String date;

    public ItemFood(String item, int energy, String date) {
        name = item;
        cal = energy;
        this.date = date;
    }

    public ItemFood(String item, int energy) {
        name = item;
        cal = energy;
    }
    public String getItemName(){
        return name;
    }
    public int getCalories(){
        return cal;
    }
    public void setDate(String d) { date = d; }
    public String getDate() { return date; }

    @Override
    public String toString() { return name + " " + cal + " " + date; }
}
