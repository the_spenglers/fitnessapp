package edu.csun.a380group5.fitnessapp.WorkoutGenerator;
/*
*
* The Fitness App
*  Group 5
* Timothy Spengler, Joshua Benz, Kodi Winterer, Alex Melendez-White
* Comp 380 Spring 2017
*
*/
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.io.IOException;

import edu.csun.a380group5.fitnessapp.MainActivity;
import edu.csun.a380group5.fitnessapp.R;
import edu.csun.a380group5.fitnessapp.Utils;

interface AsyncResponse {
    void processFinish(String result);
}

public class YoutubeActivity extends YouTubeBaseActivity implements  YouTubePlayer.OnInitializedListener, View.OnClickListener {
    private YouTubePlayerView youTubePlayerView;
    private String keyword = new String();

    private static final String API_KEY = "AIzaSyApp1T-3-WjCpawBi5fhZqjyfnN-10nxR0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube);

        Button button_home = (Button) findViewById(R.id.button_home);
        button_home.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null) { keyword = (bundle.getString("exerciseName")); }

        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        youTubePlayerView.initialize(API_KEY, this);
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer youTubePlayer, boolean b) {
        keyword = keyword + " workout tutorial";
        keyword = keyword.replace(" ", "+");
        String url = "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=1&order=viewCount&q=" + keyword + "&type=video&key=" + API_KEY;

        SearchYoutube searchYoutube = new SearchYoutube(new AsyncResponse() {
            @Override
            public void processFinish(String searchResults) {
                if(searchResults == null) {
                    Toast.makeText(getApplicationContext(), "No Search Results Found!", Toast.LENGTH_SHORT).show();
                    YoutubeActivity.super.onBackPressed();
                } else {
                    youTubePlayer.loadVideo(Utils.removeQuotes(searchResults));
                }
            }
        });
        searchYoutube.execute(url);
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Something went wrong!", Toast.LENGTH_SHORT).show();
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch(v.getId()) {
            case R.id.button_home:
                intent = new Intent(v.getContext(), MainActivity.class);
                v.getContext().startActivity(intent);
                break;
            default:
                break;
        }
    }

    private class SearchYoutube extends AsyncTask<String, Void, String> {
        public AsyncResponse delegate = null;//Call back interface

        public SearchYoutube(AsyncResponse asyncResponse) {
            delegate = asyncResponse;//Assigning call back interfacethrough constructor
        }

        @Override
        protected String doInBackground( String... url) {
            String videoId = new String();
            try {
                Document doc = Jsoup.connect(url[0]).ignoreContentType(true).timeout(10 * 1000).get();
                String jsonResult = doc.text();

                JsonObject gsonObject = new Gson().fromJson(jsonResult, JsonObject.class);
                JsonArray gsonArray = gsonObject.getAsJsonArray("items");

                if(gsonArray.size() == 0) { return null; } //no search results

                videoId = gsonArray.get(0).getAsJsonObject().getAsJsonObject("id").get("videoId").toString();

            } catch (IOException e) {
                e.printStackTrace();
            }
            return videoId;
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            delegate.processFinish(result);
        }
    }
}
