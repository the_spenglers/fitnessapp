package edu.csun.a380group5.fitnessapp.WorkoutGenerator;
/*
*
* The Fitness App
*  Group 5
* Timothy Spengler, Joshua Benz, Kodi Winterer, Alex Melendez-White
* Comp 380 Spring 2017
*
*/
import android.os.Parcel;
import android.os.Parcelable;
import java.util.UUID;

public class ExerciseStruct implements Parcelable {
    private String name;
    private String bodyPart;
    private String url;
    private String id;

    public ExerciseStruct() {
        name         = new String();
        bodyPart     = new String();
        url          = new String();
        id           = new String();
    }

    public ExerciseStruct(String name, String bodyPart, String url, String id) {
        this.name       = name;
        this.bodyPart   = bodyPart;
        this.url        = url;
        this.id         = id;
    }

    public ExerciseStruct(String name, String bodyPart, String url) {
        this.name       = name;
        this.bodyPart   = bodyPart;
        this.url        = url;
        id              = UUID.randomUUID().toString();;

    }

    public ExerciseStruct(String name, String bodyPart) {
        this.name       = name;
        this.bodyPart   = bodyPart;
        id              = UUID.randomUUID().toString();;
        url             = new String();

    }

    public String getName()             { return name;      }
    public String getUrl()              { return url;       }
    public String getId()               { return id;        }
    public String getBodyPart()         { return  bodyPart; }
    public void setName(String name)    { this.name = name; }
    public void setUrl(String url)      { this.url = url;   }
    public void setId(String id)        { this.id = id;     }
    public void setBodyPart(String b)   { bodyPart = b;     }

    @Override
    public String toString() { return "" + id + ""+ name + "" + bodyPart + "" + url + "\n"; }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(bodyPart);
        dest.writeString(url);
        dest.writeString(id);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ExerciseStruct createFromParcel(Parcel in) {
            return new ExerciseStruct(in);
        }
        public ExerciseStruct[] newArray(int size) {
            return new ExerciseStruct[size];
        }
    };

    public ExerciseStruct(Parcel in) {
        name        = in.readString();
        bodyPart    = in.readString();
        url         = in.readString();
        id          = in.readString();
    }
}