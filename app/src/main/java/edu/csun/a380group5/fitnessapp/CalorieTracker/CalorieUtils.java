package edu.csun.a380group5.fitnessapp.CalorieTracker;
/*
*
* The Fitness App
*  Group 5
* Timothy Spengler, Joshua Benz, Kodi Winterer, Alex Melendez-White
* Comp 380 Spring 2017
*
*/
import android.content.Context;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import edu.csun.a380group5.fitnessapp.LoadSaveUtils;

public class CalorieUtils {
    private static Map<String, List<ItemFood>> map = null;

    public static Map<String, List<ItemFood>> getCaloriesMap(Context c) {
        if(map == null)
            map = LoadSaveUtils.loadFromFile(Calendar.getInstance().get(Calendar.YEAR),c);

        return map;
    }

    public static void setCaloriesMap(Map<String, List<ItemFood>> m) { map = m; }

}
