package edu.csun.a380group5.fitnessapp.CalorieTracker;
/*
*
* The Fitness App
*  Group 5
* Timothy Spengler, Joshua Benz, Kodi Winterer, Alex Melendez-White
* Comp 380 Spring 2017
*
*/
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import edu.csun.a380group5.fitnessapp.LoadSaveUtils;
import edu.csun.a380group5.fitnessapp.R;
import edu.csun.a380group5.fitnessapp.Utils;

public class CalorieTrackerActivity extends AppCompatActivity  {

    private SearchView searchItem;
    private Map<String, List<ItemFood>> caloriesMap;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calorie_menu);
        caloriesMap = CalorieUtils.getCaloriesMap(this);

        final ListView calorielist = (ListView) findViewById(android.R.id.list);

        searchItem = (SearchView) findViewById(R.id.search_item);
        searchItem.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(Utils.isNetworkAvailable(getApplicationContext())) {
                    Intent intent = new Intent(searchItem.getContext(), CalorieSearchActivity.class);
                    intent.putExtra("term", searchItem.getQuery() + "");
                    searchItem.getContext().startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "No Network Connection!", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }});

        CalendarView calendarView = (CalendarView) findViewById(R.id.calendar_view);

        ItemFood itemFood = null;
        Intent intent = getIntent();
        if(intent.hasExtra("name") && intent.hasExtra("value")) {
            itemFood = new ItemFood(intent.getStringExtra("name"), Integer.parseInt(intent.getStringExtra("value")));
            itemFood.setDate(Utils.parseDateAsString(new Date()));

            if(caloriesMap.containsKey(itemFood.getDate())) {
                caloriesMap.get(itemFood.getDate()).add(itemFood);
            } else {
                caloriesMap.put(itemFood.getDate(), new ArrayList<ItemFood>());
                caloriesMap.get(itemFood.getDate()).add(itemFood);
            }

            int[] itemDate = Utils.parseDateAsIntArray(itemFood.getDate());
            try {
                LoadSaveUtils.saveToFile(caloriesMap, itemDate[2], this);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Toast.makeText(getApplicationContext(), ""+itemFood.getItemName()+":"+itemFood.getCalories(), Toast.LENGTH_SHORT).show();
        }

        final List<String> names = new ArrayList<String>();
        int totalcal = 0;

        if(!caloriesMap.isEmpty()) {
            final List<ItemFood> temp = caloriesMap.get(Utils.parseDateAsString(new Date()));

            if(temp != null) {
                for (int i = 0; i < temp.size(); i++) {
                    String name = temp.get(i).getItemName();
                    int cals = temp.get(i).getCalories();
                    totalcal = totalcal + cals;
                    names.add(name + "\t\t cals = " + cals);
                }
            }
        }

            names.add("total calories: " + totalcal);
            final ArrayAdapter<String> adapter = new ArrayAdapter<String>(CalorieTrackerActivity.this, android.R.layout.simple_list_item_1, names);
            calorielist.setAdapter(adapter);

            calorielist.setLongClickable(true);
            calorielist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                    if (names.get(position).contains("total")) {
                        Toast.makeText(getApplicationContext(), "Cannot Delete This!", Toast.LENGTH_SHORT).show();
                        return false;
                    }

                    AlertDialog.Builder alertDelete = new AlertDialog.Builder(CalorieTrackerActivity.this);
                    alertDelete.setTitle("Delete");
                    alertDelete.setMessage("Delete Item?");

                    alertDelete.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            String tempDate = Utils.parseDateAsString(new Date());
                            if (caloriesMap.containsKey(tempDate)) {
                                ItemFood item = caloriesMap.get(tempDate).get(position);
                                caloriesMap.get(tempDate).remove(position);
                                names.remove(position);

                                String[] tCals = names.get(names.size() - 1).split(":");

                                int newTotalCals = Integer.parseInt(tCals[1].trim());
                                names.remove(names.size() - 1);
                                newTotalCals = newTotalCals - item.getCalories();
                                names.add("total calories: " + newTotalCals);
                                adapter.notifyDataSetChanged();

                                int[] itemDate = Utils.parseDateAsIntArray(tempDate);
                                try {
                                    LoadSaveUtils.saveToFile(caloriesMap, itemDate[2], getApplicationContext());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                Toast.makeText(getApplicationContext(), "Item Deleted", Toast.LENGTH_SHORT).show();
                            }
                            dialog.dismiss();
                        }
                    });

                    alertDelete.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    alertDelete.show();
                    return true;
                }
            });


        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                month = month+1;
                LinearLayout linearLayout =  (LinearLayout) findViewById(R.id.calorie_items);
                names.removeAll(names);
                adapter.notifyDataSetChanged();

                Toast.makeText(getApplicationContext(), ""+month+"-"+dayOfMonth+"-"+year, Toast.LENGTH_SHORT).show();
                String tempMonth = ""+month;
                String tempDay = ""+dayOfMonth;

                if(month < 10){
                    tempMonth = "0"+month;
                }
                if(dayOfMonth < 10){
                    tempDay = "0"+dayOfMonth;
                }
                final String tempDate = year+"-"+tempMonth+"-"+tempDay;

                if(caloriesMap.containsKey(tempDate)) {
                    List<ItemFood> temp = caloriesMap.get(tempDate);
                    int totalcal = 0;

                    for (int i = 0; i < temp.size(); i++) {
                        String name = temp.get(i).getItemName();
                        int cals = temp.get(i).getCalories();
                        totalcal = cals + totalcal;
                        names.add(name + "\t\t cals = " + cals);
                    }
                    names.add("total calories = "+totalcal);
                }
                adapter.notifyDataSetChanged();

                calorielist.setLongClickable(true);
                calorielist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                        if(names.get(position).contains("total")) {
                            Toast.makeText(getApplicationContext(), "Cannot Delete This!", Toast.LENGTH_SHORT).show();
                            return false;
                        }

                        AlertDialog.Builder alertDelete = new AlertDialog.Builder(CalorieTrackerActivity.this);
                        alertDelete.setTitle("Delete");
                        alertDelete.setMessage("Delete Item?");

                        alertDelete.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(caloriesMap.containsKey(tempDate)) {
                                    ItemFood item = caloriesMap.get(tempDate).get(position);
                                    caloriesMap.get(tempDate).remove(position);
                                    names.remove(position);
                                    int newTotalCals = 0;
                                    String[] tCals = names.get(names.size()-1).split(":");
                                    if(tCals.length > 1) {
                                        newTotalCals = Integer.parseInt(tCals[1].trim());
                                    }

                                    names.remove(names.size()-1);
                                    newTotalCals = newTotalCals - item.getCalories();
                                    names.add("total calories: "+newTotalCals);
                                    adapter.notifyDataSetChanged();

                                    int[] itemDate = Utils.parseDateAsIntArray(tempDate);
                                    try {
                                        LoadSaveUtils.saveToFile(caloriesMap, itemDate[2], getApplicationContext());
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    Toast.makeText(getApplicationContext(), "Item Deleted", Toast.LENGTH_SHORT).show();
                                }
                                dialog.dismiss();
                            }
                        });

                        alertDelete.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        alertDelete.show();
                        return true;
                    }
                });

            }
        });
    }
}
