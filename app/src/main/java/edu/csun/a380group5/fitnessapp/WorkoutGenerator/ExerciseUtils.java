package edu.csun.a380group5.fitnessapp.WorkoutGenerator;
/*
*
* The Fitness App
*  Group 5
* Timothy Spengler, Joshua Benz, Kodi Winterer, Alex Melendez-White
* Comp 380 Spring 2017
*
*/
import android.content.Context;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;

import java.util.List;

import edu.csun.a380group5.fitnessapp.R;

public class ExerciseUtils {
    private static Context context;
    private static List<String[]>   abdominals   = null,
                                    biceps       = null,
                                    calves       = null,
                                    chest        = null,
                                    forearms     = null,
                                    glutes       = null,
                                    hamstrings   = null,
                                    lats         = null,
                                    lower_back   = null,
                                    middle_back  = null,
                                    neck         = null,
                                    quadriceps   = null,
                                    shoulders    = null,
                                    traps        = null,
                                    triceps      = null;

    public static void setExerciseUtilContext(Context c) { context = c; }

    //if the list was already loaded, return it, otherwise load it from the csv file and then return it
    public static List<String[]> getList(String exerciseName) {
        switch (exerciseName.toLowerCase()) {
            case "abdominals" : if(abdominals   == null) { loadFromFile("abdominals") ; } return abdominals ;
            case "biceps"     : if(biceps       == null) { loadFromFile("biceps")     ; } return biceps     ;
            case "calves"     : if(calves       == null) { loadFromFile("calves")     ; } return calves     ;
            case "chest"      : if(chest        == null) { loadFromFile("chest")      ; } return chest      ;
            case "forearms"   : if(forearms     == null) { loadFromFile("forearms")   ; } return forearms   ;
            case "glutes"     : if(glutes       == null) { loadFromFile("glutes")     ; } return glutes     ;
            case "hamstrings" : if(hamstrings   == null) { loadFromFile("hamstrings") ; } return hamstrings ;
            case "lats"       : if(lats         == null) { loadFromFile("lats")       ; } return lats       ;
            case "lower_back" : if(lower_back   == null) { loadFromFile("lower_back") ; } return lower_back ;
            case "middle_back": if(middle_back  == null) { loadFromFile("middle_back"); } return middle_back;
            case "neck"       : if(neck         == null) { loadFromFile("neck")       ; } return neck       ;
            case "quadriceps" : if(quadriceps   == null) { loadFromFile("quadriceps") ; } return quadriceps ;
            case "shoulders"  : if(shoulders    == null) { loadFromFile("shoulders")  ; } return shoulders  ;
            case "traps"      : if(traps        == null) { loadFromFile("traps")      ; } return traps      ;
            case "triceps"    : if(triceps      == null) { loadFromFile("triceps")    ; } return triceps    ;

            default:
                break;
        }
        return null;
    }

    private static void loadFromFile(String name) {
        CsvParserSettings csvSettings = new CsvParserSettings();
        csvSettings.getFormat().setLineSeparator("\n");
        CsvParser parser = new CsvParser(csvSettings);

        switch (name.toLowerCase()) {
            case "abdominals" : abdominals  = parser.parseAll(context.getResources().openRawResource(R.raw.abdominals)) ;    break;
            case "biceps"     : biceps      = parser.parseAll(context.getResources().openRawResource(R.raw.biceps))     ;    break;
            case "calves"     : calves      = parser.parseAll(context.getResources().openRawResource(R.raw.calves))     ;    break;
            case "chest"      : chest       = parser.parseAll(context.getResources().openRawResource(R.raw.chest))      ;    break;
            case "forearms"   : forearms    = parser.parseAll(context.getResources().openRawResource(R.raw.forearms))   ;    break;
            case "glutes"     : glutes      = parser.parseAll(context.getResources().openRawResource(R.raw.glutes))     ;    break;
            case "hamstrings" : hamstrings  = parser.parseAll(context.getResources().openRawResource(R.raw.hamstrings)) ;    break;
            case "lats"       : lats        = parser.parseAll(context.getResources().openRawResource(R.raw.lats))       ;    break;
            case "lower_back" : lower_back  = parser.parseAll(context.getResources().openRawResource(R.raw.lower_back)) ;    break;
            case "middle_back": middle_back = parser.parseAll(context.getResources().openRawResource(R.raw.middle_back));    break;
            case "neck"       : neck        = parser.parseAll(context.getResources().openRawResource(R.raw.neck))       ;    break;
            case "quadriceps" : quadriceps  = parser.parseAll(context.getResources().openRawResource(R.raw.quadriceps)) ;    break;
            case "shoulders"  : shoulders   = parser.parseAll(context.getResources().openRawResource(R.raw.shoulders))  ;    break;
            case "traps"      : traps       = parser.parseAll(context.getResources().openRawResource(R.raw.traps))      ;    break;
            case "triceps"    : triceps     = parser.parseAll(context.getResources().openRawResource(R.raw.triceps))    ;    break;

            default:
                break;
        }
    }
}
