package edu.csun.a380group5.fitnessapp.CalorieTracker;
/*
*
* The Fitness App
*  Group 5
* Timothy Spengler, Joshua Benz, Kodi Winterer, Alex Melendez-White
* Comp 380 Spring 2017
*
*/
import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import edu.csun.a380group5.fitnessapp.R;
import edu.csun.a380group5.fitnessapp.Utils;

interface AsyncResponse {
    void processFinish(String result);
}
public class CalorieSearchActivity extends  ListActivity implements Serializable {
    private List<String> items;
    private List<String> itemId;
    private ListView list;
    private ArrayAdapter<String> adapter;
    private final String baseURL = "https://api.nal.usda.gov/ndb/search/?format=json&q=";
    private final String endURL = "&sort=n&max=50&offset=0&api_key=NggHlTPnWeJmXtBq0r5FBrmxY010wpEE07ekLOUc";
    private final String searchURL = "https://api.nal.usda.gov/ndb/nutrients/?format=json&api_key=NggHlTPnWeJmXtBq0r5FBrmxY010wpEE07ekLOUc&nutrients=208&ndbno=";
    private String searchTerm;
    private String url;
    private String value;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calorie_search);
        final Button button = (Button) findViewById(R.id.button2);
        button.setVisibility(View.GONE);
        final EditText textv = (EditText) findViewById(R.id.editText);
        final EditText textv2 = (EditText) findViewById(R.id.editText2);
        textv.setVisibility(View.GONE);
        textv2.setVisibility(View.GONE);
        final Intent intent = getIntent();
        list = (ListView) findViewById(android.R.id.list);
        searchTerm = (intent.getStringExtra("term"));
        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView av, View v, final int index, long arg3){
                String id = itemId.get(index);
                id = Utils.removeQuotes(id);

                String nutrientURL = searchURL+id;
                SearchItemDatabase searchItemDatabase = new SearchItemDatabase(new AsyncResponse() {
                    @Override
                    public void processFinish(String searchResults) {
                        if(searchResults == null) {
                            Toast.makeText(getApplicationContext(), "No Search Results Found!", Toast.LENGTH_SHORT).show();
                            CalorieSearchActivity.super.onBackPressed();
                        } else {
                            searchResults = Utils.removeQuotes(searchResults);
                            Intent intent = new Intent(list.getContext(), CalorieTrackerActivity.class);
                            intent.putExtra("name", Utils.removeQuotes(items.get(index)));
                            intent.putExtra("value", searchResults);
                            list.getContext().startActivity(intent);
                        }
                    }
                });
                searchItemDatabase.execute(nutrientURL);

                return true;
            }});
        items =  new ArrayList<String>();
        itemId = new ArrayList<String>();
        url = baseURL+searchTerm+endURL;

        SearchDatabase searchDatabase = new SearchDatabase(new AsyncResponse() {
            @Override
            public void processFinish(String searchResults) {
                if(searchResults == null) {
                    Toast.makeText(getApplicationContext(), "No Search Results Found!", Toast.LENGTH_SHORT).show();
                    textv.setVisibility(View.VISIBLE);
                    textv2.setVisibility(View.VISIBLE);
                    button.setVisibility(View.VISIBLE);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view){
                            Intent intent1 = new Intent(view.getContext(), CalorieTrackerActivity.class);
                            intent1.putExtra("name",textv.getText().toString());
                            intent1.putExtra("value",textv2.getText().toString());
                            view.getContext().startActivity(intent1);
                        }
                    });
                }
            }
        });
        searchDatabase.execute(url);
    }
//--------------------------------------------------------------------------------------------------
    private class SearchDatabase extends AsyncTask<String, Void, String> {
        public AsyncResponse delegate = null;//Call back interface
        public SearchDatabase(AsyncResponse asyncResponse) {
            delegate = asyncResponse;
            //Assigning call back interface through constructor
        }

        @Override
        protected String doInBackground( String... url) {
            String itemNames = null;
            String itemNdbno;
            try {
                Document doc = Jsoup.connect(url[0]).ignoreContentType(true).timeout(10 * 1000).get();
                String results = doc.text();
                JsonObject gsonObject = new Gson().fromJson(results, JsonObject.class);
                if(gsonObject.getAsJsonObject("list") != null) {
                    JsonArray gsonArray = gsonObject.getAsJsonObject("list").getAsJsonArray("item");
                    if (gsonArray.size() == 0) {
                        items.add("Item could not be found!");
                        return null;
                    } //no search results
                    int end = gsonObject.getAsJsonObject("list").getAsJsonPrimitive("end").getAsInt();
                    for (int i = 0; i < end; i++) {
                        itemNames = gsonArray.get(i).getAsJsonObject().getAsJsonPrimitive("name").toString();
                        itemNdbno = gsonArray.get(i).getAsJsonObject().getAsJsonPrimitive("ndbno").toString();
                        items.add(itemNames);
                        itemId.add(itemNdbno);
                    }
                }
                else{
                    items.add("Item could not be found!!");
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch(JsonParseException f){
                items.add("Item could not be found!");
                return null;
            }
            return itemNames;
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            delegate.processFinish(result);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(CalorieSearchActivity.this, android.R.layout.simple_list_item_1,items);
            list.setAdapter(adapter);
        }
    }// end SearchDatabase class

    // ---------------------------------------------------------------------------------------------
    private class SearchItemDatabase extends AsyncTask<String, Void, String> {
        public AsyncResponse delegate = null;//Call back interface
        public SearchItemDatabase(AsyncResponse asyncResponse) {
            delegate = asyncResponse;
            //Assigning call back interface through constructor
        }

        @Override
        protected String doInBackground( String... url) {
            String cals = null;
            try {
                Document doc = Jsoup.connect(url[0]).ignoreContentType(true).timeout(10 * 1000).get();
                String results = doc.text();
                JsonObject gsonObject = new Gson().fromJson(results, JsonObject.class);

                if(gsonObject.getAsJsonObject("report") != null) {
                    JsonArray gsonArray = gsonObject.getAsJsonObject("report").getAsJsonArray("foods");
                    cals = gsonArray.get(0).getAsJsonObject().getAsJsonArray("nutrients").get(0).getAsJsonObject().getAsJsonPrimitive("value").getAsString();

                    if (gsonArray.size() == 0) {
                        return null;
                    } //no search results
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return cals;
        }
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            delegate.processFinish(result);
        }
    }
}// end SearchItemDatabase class

