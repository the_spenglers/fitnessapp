package edu.csun.a380group5.fitnessapp;
/*
*
* The Fitness App
*  Group 5
* Timothy Spengler, Joshua Benz, Kodi Winterer, Alex Melendez-White
* Comp 380 Spring 2017
*
*/
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Utils {

    //checks if device is connected to a network
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static String removeQuotes(String s) {
        if(!s.contains("\"")) { return  s; }
        return s.substring(1, s.length()-1); //remove the ""
    }

    public static int[] parseDateAsIntArray(Date date) {
        String sDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        String[] array = sDate.split("-");
        int[] result = new int[3];

        result[0] = Integer.parseInt(array[2]);//day
        result[1] = Integer.parseInt(array[1]);//month
        result[2] = Integer.parseInt(array[0]);//year

        return result;
    }

    public static int[] parseDateAsIntArray(String date) {
        String[] array = date.split("-");
        int[] result = new int[3];

        result[0] = Integer.parseInt(array[2]);//day
        result[1] = Integer.parseInt(array[1]);//month
        result[2] = Integer.parseInt(array[0]);//year

        return result;
    }

    public static String parseDateAsString(Date date) {
        String sDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        return sDate;
    }
}
