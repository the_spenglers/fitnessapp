package edu.csun.a380group5.fitnessapp;
/*
*
* The Fitness App
*  Group 5
* Timothy Spengler, Joshua Benz, Kodi Winterer, Alex Melendez-White
* Comp 380 Spring 2017
*
*/

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import edu.csun.a380group5.fitnessapp.CalorieTracker.CalorieTrackerActivity;
import edu.csun.a380group5.fitnessapp.WorkoutGenerator.WorkoutGeneratorActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonWorkoutGenerator = (Button) findViewById(R.id.button_workout_generator);
        Button calorieButton = (Button) findViewById(R.id.calorie_button);

        buttonWorkoutGenerator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), WorkoutGeneratorActivity.class);
                view.getContext().startActivity(intent);}
        });

        calorieButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentCalorie = new Intent(view.getContext(), CalorieTrackerActivity.class);
                view.getContext().startActivity(intentCalorie);
            }
        });
    }
}

