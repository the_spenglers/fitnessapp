package edu.csun.a380group5.fitnessapp.WorkoutGenerator;
/*
*
* The Fitness App
*  Group 5
* Timothy Spengler, Joshua Benz, Kodi Winterer, Alex Melendez-White
* Comp 380 Spring 2017
*
*/
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;


/*
   * Notes from Android Reference: "AsyncTask enables proper and easy use of the UI thread.
   * This class allows you to perform background operations and publish results on the UI thread
   * without having to manipulate threads and/or handlers."
   *
   * Must be subclassed: AsyncTask Parameters:
   * "1. Params, the type of the parameters sent to the task upon execution.
   *  2. Progress, the type of the progress units published during the background computation.
   *  3. Result, the type of the result of the background computation."
   *
   * Rules for using AsyncTask:
   * "The AsyncTask class must be loaded on the UI thread. This is done automatically as of JELLY_BEAN.
   *  The task instance must be created on the UI thread.
   *  execute(Params...) must be invoked on the UI thread.
   *  Do not call onPreExecute(), onPostExecute(Result), doInBackground(Params...), onProgressUpdate(Progress...) manually.
   *  The task can be executed only once (an exception will be thrown if a second execution is attempted.)"
*/

public class WorkoutGenerator extends AsyncTask<String, Integer, List<ExerciseStruct>> {
    private String params;
    private Context context;

    WorkoutGenerator(String s, Context c) {
        params = s;
        context = c;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected List<ExerciseStruct> doInBackground(String... str) {
        ExerciseUtils.setExerciseUtilContext(context);
        List<ExerciseStruct> exerciseList = new ArrayList<ExerciseStruct>();
        List<String[]> list;
        Random random = new Random();
        int exerciseIndex = 0;

        String[] theParams = params.split(":");

        for(String s : theParams) {
            list = ExerciseUtils.getList(s);

            if(list != null) {
                exerciseIndex = random.nextInt(list.size());
                exerciseList.add(new ExerciseStruct(list.get(exerciseIndex)[0], s));
                int duplicates = hasDuplicates(exerciseList, list.size(), exerciseIndex);
                if(duplicates != -1) {
                    exerciseList.remove(exerciseList.size()-1);
                    exerciseList.add(new ExerciseStruct(list.get(duplicates)[0], s));
                }
            }
        }
        return exerciseList;
    }

    protected  void onPostExecute(List<ExerciseStruct> exerciseStructs) {
        super.onPostExecute(exerciseStructs);
        if(exerciseStructs != null) {
            ArrayList<ExerciseStruct> data = new ArrayList<ExerciseStruct>(exerciseStructs.size());
            for (ExerciseStruct e : exerciseStructs) {
                data.add(e);
            }
            Intent intent = new Intent(context, ExerciseListActivity.class);
            intent.putParcelableArrayListExtra("data.content", data);

            context.startActivity(intent);
        }
    }

    private int hasDuplicates(List<ExerciseStruct> list, int possNumUnique, int index) {
        int newIndex = -1;

        if(list.size() >= possNumUnique)  return -1;  //pigeon hole principle?

        List<String> nameList = new ArrayList<String>(list.size());

        for(ExerciseStruct e : list) { nameList.add(e.getName()); }
        Set<String> nameSet = new HashSet<String>(nameList); //takes advantage of definition of a Set. Unique elements

        if(nameList.size() == nameSet.size()) return -1;
        else {
            Random random = new Random();
            newIndex = random.nextInt(possNumUnique);
            while(newIndex == index) { newIndex = random.nextInt(possNumUnique); }
        }
        return newIndex;
    }
}